from compte import CompteSimple
from compte import Personne
from compte_courant import CompteCourant

def test_crediter():
    personne = Personne("Lam")
    compte = CompteCourant(personne, 1000)
    assert compte.solde == 1000
    compte.crediter(1000)
    assert compte.solde == 2000

def test_debiter_solde_positif():
    personne = Personne("Lam")
    compte = CompteCourant(personne, 1000)
    assert compte.solde == 1000
    compte.debiter(500)
    assert compte.solde == 500


def test_debiter_solde_negatif():
    personne = Personne("Lam")
    compte = CompteCourant(personne, 1000)
    assert compte.solde == 1000
    compte.debiter(2000)
    assert compte.solde == -1000

def test_releve_de_comte():
    personne = Personne("Lam")
    compte = CompteCourant(personne, 1000)
    compte.crediter(2000)
    compte.debiter(10)
    compte.debiter(10)
    compte.debiter(100)
    compte.debiter(50)
    compte.crediter(30)
    compte.releve_de_compte()