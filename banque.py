from compte import CompteSimple
from compte_courant import CompteCourant

class Banque:
    def __init__(self):
        # self.numero_compte_actuel = 10000                                         Cas où le numéro de compte est géré par la classe Banque
        self.comptes = []

    def ouvrir_compte(self, titulaire, depot):
        Cas où le numéro de compte est géré par la classe Banque
        compte = CompteSimple(titulaire, depot)
        # compte = CompteSimple(titulaire, depot, self.numero_compte_actuel)        Cas où le numéro de compte est géré par la classe Banque
        # self.numero_compte_actuel += 1                                            Cas où le numéro de compte est géré par la classe Banque
        self.comptes.append(compte)

    def ouvrir_compte_courant(self, titulaire, depot):
        compte = CompteCourant(titulaire, depot)
        # compte = CompteCourant(titulaire, depot, self.numero_compte_actuel)       Cas où le numéro de compte est géré par la classe Banque
        # self.numero_compte_actuel += 1                                            Cas où le numéro de compte est géré par la classe Banque
        self.comptes.append(compte)

    def total_argent(self):
        total = 0
        for compte in self.comptes:
            if compte.solde > 0:
                total += compte.solde

        return total

    def prelever_frais(self, frais):
        for compte in self.comptes:
            compte.debiter(frais)

    def editer(self):
        for compte in self.comptes:
            if type(compte) is CompteCourant:
                compte.releve_de_compte()
