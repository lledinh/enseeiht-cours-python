class Personne:
    def __init__(self, nom):
        self.nom = nom

class CompteSimple:
    # self.numero_compte_actuel = 10000                           Cas où le numéro de compte est géré par la classe CompteSimple et CompteCourant

    def __init__(self, titulaire, depot = 0):
        self.titulaire = titulaire
        self.__solde = depot
        # self.numero_compte = self.numero_compte_actuel          Cas où le numéro de compte est géré par la classe CompteSimple et CompteCourant
        # self.numero_compte_actuel += 1                          Cas où le numéro de compte est géré par la classe CompteSimple et CompteCourant

    '''
    def __init__(self, titulaire, numero_compte, depot = 0):      Cas où le numéro de compte est géré par la classe Banque
        self.titulaire = titulaire
        self.numero_compte = numero_compte
        self.__solde = depot
    '''

    def crediter(self, montant):
        self.__solde += montant

    def debiter(self, montant):
        self.__solde -= montant
        
    @property
    def solde(self):
        return self.__solde
        


    