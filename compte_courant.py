from compte import CompteSimple

class CompteCourant(CompteSimple):
    def __init__(self, titulaire, depot = 0):
        super().__init__(titulaire, depot)
        self.historique = []
        self.historique.append(("credit", depot))

    def crediter(self, montant):
        super().crediter(montant)
        self.historique.append(("credit", montant))

    def debiter(self, montant):
        super().debiter(montant)
        self.historique.append(("debit", montant))

    def releve_de_compte(self):
        for operation in self.historique:
            print ("{0} de {1} euros".format(operation[0], operation[1]))

        print ("Solde final: {0}".format(self.solde))