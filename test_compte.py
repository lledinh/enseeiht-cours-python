from compte import CompteSimple
from compte import Personne

def test_crediter():
    personne = Personne("Lam")
    compte = CompteSimple(personne, 1000)
    assert compte.solde == 1000
    compte.crediter(1000)
    assert compte.solde == 2000

def test_debiter_solde_positif():
    personne = Personne("Lam")
    compte = CompteSimple(personne, 1000)
    assert compte.solde == 1000
    compte.debiter(500)
    assert compte.solde == 500


def test_debiter_solde_negatif():
    personne = Personne("Lam")
    compte = CompteSimple(personne, 1000)
    assert compte.solde == 1000
    compte.debiter(2000)
    assert compte.solde == -1000