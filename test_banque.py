from compte import CompteSimple
from compte import Personne
from banque import Banque

def test_ouvrir_compte():
    banque = Banque()

    personne = Personne("Lam")
    banque.ouvrir_compte(personne, 1000)

    assert len(banque.comptes) == 1
    assert banque.comptes[0].titulaire.nom == "Lam"
    assert banque.comptes[0].solde == 1000

def test_total_argent():
    banque = Banque()
    
    personne = Personne("Lam")
    banque.ouvrir_compte(personne, 1000)
    personne = Personne("Xavier")
    banque.ouvrir_compte(personne, 1000)

    assert banque.total_argent() == 2000


def test_prelever_frais():
    banque = Banque()
    
    personne = Personne("Lam")
    banque.ouvrir_compte(personne, 1000)
    personne = Personne("Xavier")
    banque.ouvrir_compte(personne, 1000)


    assert banque.total_argent() == 2000

    banque.prelever_frais(10)

    assert banque.total_argent() == 1980

def test_ouvrir_compte_courant():
    banque = Banque()

    personne = Personne("Lam")
    banque.ouvrir_compte_courant(personne, 1000)

    assert len(banque.comptes) == 1
    assert banque.comptes[0].titulaire.nom == "Lam"
    assert banque.comptes[0].solde == 1000

def test_total_argent_compte_courant():
    banque = Banque()
    
    personne = Personne("Lam")
    banque.ouvrir_compte_courant(personne, 1000)
    personne = Personne("Xavier")
    banque.ouvrir_compte_courant(personne, 1000)

    assert banque.total_argent() == 2000


def test_prelever_frais_compte_courant():
    banque = Banque()
    
    personne = Personne("Lam")
    banque.ouvrir_compte_courant(personne, 1000)
    personne = Personne("Xavier")
    banque.ouvrir_compte_courant(personne, 1000)


    assert banque.total_argent() == 2000

    banque.prelever_frais(10)

    assert banque.total_argent() == 1980


def test_editer_compte_courant():
    banque = Banque()
    
    personne = Personne("Lam")
    banque.ouvrir_compte_courant(personne, 1000)
    personne = Personne("Xavier")
    banque.ouvrir_compte_courant(personne, 1000)
    personne = Personne("Leo")
    banque.ouvrir_compte(personne, 1000)

    banque.editer()